# React Boilerplate

### Features:

- Uses React library as a base
- Enables newest CSS syntax and features via PostCSS with modules
- Provides types support via TypeScript
- Has optimized production build via Terser
- Has build-in component for rendering svg icons
- Has many lint rules which take care of code syntax quality
- Has Prettier support out of the box
- Ships with tests environment based on Jest and react-testing-library
- Automatically generates manifest.json along with favicons for all environments

### Additional info:

React, ract-dom and prettier should have exact version of packages
