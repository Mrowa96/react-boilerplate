module.exports = {
  tabWidth: 2,
  semi: true,
  singleQuote: true,
  trailingComma: 'all',
  printWidth: 80,
  jsxBracketSameLine: true,
  endOfLine: 'lf',
};
